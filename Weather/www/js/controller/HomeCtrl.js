function HomeCtrl($scope, $http, GeolocationService) {

    $scope.champRecherche1 = 75056
    $scope.loader = false;
    $scope.panel = 0;
    $scope.search = function () {
        var url = "https://odata-inpn.mnhn.fr/taxa?size=50&embed=OPERATIONAL_GROUPS,GENERAL_PUBLIC_GROUP,DESCRIPTION,PHOTOS&hasPhoto=true&occurrenceLocationId=INSEEC" + $scope.champRecherche1 + "";
        $scope.loader = true;
        // disableHomeForm();
        $http.get(url)
            .success(httpSuccess)
            .error(httpError);
        // enableHomeForm();
        function httpError(params) {
            $scope.loader = false;
            alert("Impossible de récupérer les information");
            enableHomeForm();
        }
        function httpSuccess(response) {
            $scope.loader = false;
            $scope.responseWS1 = response._embedded.taxa;
        }
        function enableHomeForm() {
            $(".home").removeClass("disabled");
            $(".homeloader").removeClass("loader");
            $("input").prop('disabled', false);
        }
    }

    $scope.geolocate = function () {
        var onSuccess = function (position) {
            let url = "https://geo.api.gouv.fr/communes?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&fields=code,nom,codesPostaux,surface,population,centre,contour";
            $http.get(url)
                .success(httpSuccess2)
                .error(httpError2);

            function httpSuccess2(response) {
                if (response) {
                    $scope.champRecherche1 = response[0].codesPostaux[0];
                }
            }
            function httpError2(params) {
                alert("Impossible de récupérer le code postal");
            }
        };
        var onError = function onError(error) {
            alert('code: ' + error.code + '\n' +
                'message: ' + error.message + '\n');
        }
        GeolocationService.getCurrentPosition().then(onSuccess, onError);

        // Service.methodeCallback(onSuccess, onError);
        // Service.methodePromise().then( '**successFunction**' , '**errorFunction**' );
    }
}