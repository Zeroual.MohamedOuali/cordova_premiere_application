document.addEventListener('deviceready', function () {
    // ici du code au lancement de l'application 
}, false)

var app = angular.module('app', ['ng'],
    ['$provide', function ($provide) {
        $provide.decorator('$exceptionHandler', ['$delegate', function ($delegate, $injector) {
            return function (exception, cause) {
                alert("Impossible de récupérer les information : Probleme WS");
                $(".row.ng-scope").remove();
            };
        }])
    }]
);

app.factory('GeolocationService', function ($window, $q, $rootScope) {
    var geolocation = $window.navigator.geolocation;
    var defer = $q.defer();
    return {
        getCurrentPosition: function () {
            geolocation.getCurrentPosition(
                function (position) {
                    $rootScope.$apply(function () {
                        defer.resolve(position);
                    });

                }, function () {
                    $rootScope.$apply(function () {
                        defer.reject();
                    });
                },
                { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })

            return defer.promise;
        }
    }
});

app.config(function ($routeProvider) {
    $routeProvider
        .when('/home', { templateUrl: 'partials/home.html' })
        .when('/about', { templateUrl: 'partials/about.html' })
        .otherwise({ redirectTo: '/home' });
});
